import React from 'react';

import { Card, CardTitle, CardSubtitle, CardText, CardBody } from 'reactstrap';
import Avatar from './Avatar';

import bgimg from '../assets/bgcard.jpg';

const UserCard = ({
  avatar,
  avatarSize,
  title,
  subtitle,
  text,
  children,
  className,
  ...restProps
}) => {
  const classes = 'bg-gradient-theme';

  return (
    <Card inverse className={classes}  {...restProps}>
      <CardBody className="d-flex justify-content-center align-items-center flex-column bgcardstyle" style={{backgroundImage: `url(${bgimg})`}}>
        <div className="bgcard__content">
        <Avatar src={avatar} size={avatarSize} className="mb-2" />
        <CardTitle>{title}</CardTitle>
        <CardSubtitle>{subtitle}</CardSubtitle>
        <CardText>
          <small>{text}</small>
        </CardText>
        </div>
      </CardBody>
      {children}
    </Card>
  );
};

export default UserCard;
