import React from 'react';

export const Footer = () => {
    return (
        <div className="Footer">
            <div className="FooterContent">
                <div className="FooterCopywrite">© 2019 ADV Easy. Todos os direitos reservados.</div>
            </div>
        </div>
    )
}