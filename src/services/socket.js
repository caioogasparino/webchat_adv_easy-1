import openSocket from "socket.io-client";

const socketURL = process.env.REACT_APP_SOCKET_URL;
const socket = openSocket(socketURL);

export const subscribeToChannel = ({ room, username, time }) => {
  console.log("Connecting to socket...");
  socket.emit("subscribe", { room, username, time });

  return socket;
};
