import React from "react";
import { GiftedChat, Bubble, Send, InputToolbar } from "react-web-gifted-chat";
import Header from "../components/layout/Header";
import { Footer } from "../components/layout/Footer";
import {
  Container,
  Col,
  Row,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import sendico from "../assets/ic_send.png";
import { FaRegClock } from "react-icons/fa";
import { subscribeToChannel } from "../services/socket";
import { connect } from "react-redux";

const roomData = {
  user: "Usuário de teste",
  lawyer: "Advogado de teste",
  speciality: "Fazer testes",
  room: "aXtr23@fsd",
  time: 10
};

const room = roomData.room;

class Chat extends React.Component {
  constructor(props) {
    super(props);

    const messages = JSON.parse(localStorage.getItem(`messages${room}`)) || [];

    const socket = subscribeToChannel({
      room,
      username: props.user.nome,
      time: roomData.time * 60,
    });

    this.state = {
      messages,
      modalExemplo: false,
      socket,
      clockTime: `${String(roomData.time).padStart(2, '0')}:00`,
      user: props.user
    };

    socket.on("room message", (data) => {
      this.setState((previousState) => ({
        messages: GiftedChat.append(previousState.messages, [
          {
            id: data.id,
            text: data.message,
            createdAt: new Date(),
            user: {
              id: 2,
              name: data.username,
              type: "lawyer",
            },
          },
        ]),
      }));

      localStorage.setItem(`messages${room}`, JSON.stringify(this.state.messages));
    });

    socket.on("reconnect", () => {
      if (this.state.user) {
        socket.emit("add user", this.state.user.nome);
      }
    });

    socket.on("reconnect_error", () => {});

    socket.on("disconnect", () => {});

    socket.on("user joined", (data) => {});

    // Whenever the server emits 'user left', log it in the chat body
    socket.on("user left", (data) => {});

    socket.on("one second", (data) => {
      const { timeLeft } = data;

      const minutes = String(Math.floor(timeLeft / 60)).padStart(2, "0");
      const seconds = String(timeLeft % 60).padStart(2, "0");

      this.setState((previousState) => ({
        clockTime: `${minutes}:${seconds}`,
      }));

      console.log(timeLeft);
    });

    socket.on("end connection", (data) => {
      alert("Seu tempo acabou!");
    });
  }

  componentWillMount() {
    if (this.state.messages.length === 0) {
      this.setState({
        messages: [
          {
            id: 1,
            text:
              "Seja bem vindo à sala de chat! Assim que ambas as partes estiverem conectadas a sessão irá começar!",
            createdAt: new Date(),
            user: {
              id: 2,
              name: "Adv Easy",
              type: "lawyer",
            },
          },
        ],
      });
    }
  }

  toggle = (modalType) => () => {
    this.setState({
      [modalType]: !this.state[modalType],
    });
  };

  onSend(messages = []) {
    if (!messages[0].text === "") return;

    if (this.state.socket) {
      this.state.socket.emit("send message", {
        room: messages[0].id,
        message: messages[0].text,
        username: this.state.user.name,
      });
    }

    this.setState((previousState) => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }));

    localStorage.setItem(`messages${room}`, JSON.stringify(this.state.messages));
  }

  renderBubble(props) {
    let username = props.currentMessage.user.name;
    let usertype = props.currentMessage.user.type;
    let userid = props.currentMessage.user._id;
    let prevuserid = props.previousMessage.hasOwnProperty("user")
      ? props.previousMessage.user._id
      : 0;
    let avatar;

    console.log(props);

    if (prevuserid !== userid) {
      avatar = (
        <div className="ChatAvatar">
          <div className={"ChatAvatarPic ChatAvatarPic" + usertype}>
            {username.charAt(0)}
          </div>
          <div className="ChatAvatarTxt">{username}</div>
        </div>
      );
    }

    return (
      <div className={"ChatBubble ChatBubble" + props.position}>
        {avatar}
        <div className="ChatBubbleText">
          <Bubble
            {...props}
            wrapperStyle={{
              left: {
                backgroundColor: "#f4f4f4",
                elevation: 1,
                borderRadius: 4,
                padding: 12,
              },
              right: {
                backgroundColor: "#CCAA59",
                color: "#fff",
                elevation: 1,
                borderRadius: 4,
                padding: 12,
              },
            }}
          />
        </div>
      </div>
    );
  }

  renderInputToolbar(props) {
    return (
      <div className="ChatInputWrap">
        <InputToolbar {...props} />
      </div>
    );
  }

  renderSend(props) {
    return (
      <Send {...props}>
        <img className="icosend" src={sendico} alt="enviar" />
      </Send>
    );
  }

  renderChat() {
    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={(messages) => this.onSend(messages)}
        renderAvatar={null}
        renderBubble={this.renderBubble}
        placeholder={"Escreva a mensagem"}
        alwaysShowSend={true}
        renderSend={this.renderSend}
        renderInputToolbar={this.renderInputToolbar}
        minInputToolbarHeight={130}
        user={{
          _id: 1,
          name: "Carlos (Cliente)",
          type: "user",
        }}
      />
    );
  }

  render() {
    return (
      <div className="MainContainer">
        <Header />
        <Container>
          <Row>
            <Col
              md={3}
              style={{
                display: "flex",
                flexDirection: "column",
                borderRight: "1px solid #eae5e5",
                paddingRight: "20px",
              }}
            >
              <div className="ChatInfoBox ChatInfoBoxBG">
                <div className="Timer">
                  <div className="TimerTime">
                    <FaRegClock />
                    {this.state.clockTime}
                  </div>
                </div>
              </div>
              <div className="ChatInfoBox ChatInfoBoxBG">
                <div className="Info">
                  <div className="InfoData">
                    <div className="InfoVal">{roomData.user}</div>
                    <div className="InfoLabel">Cliente</div>
                  </div>
                  <div className="InfoData">
                    <div className="InfoVal">{roomData.lawyer}</div>
                    <div className="InfoLabel">Advogado</div>
                  </div>
                  <div className="InfoData">
                    <div className="InfoVal">{roomData.speciality}</div>
                    <div className="InfoLabel">Especialidade</div>
                  </div>
                </div>
              </div>
              <div className="ChatInfoBox InfoBoxBottom">
                <p>
                  Para solicitar um <strong>Serviço Adicional</strong>, por
                  favor utilize o aplicativo ADV Easy
                </p>
                <hr />
                <button
                  className="btn btn-primaryghost"
                  onClick={this.toggle("modalExemplo")}
                >
                  Finalizar Consultoria
                </button>
              </div>

              <Modal
                isOpen={this.state.modalExemplo}
                toggle={this.toggle("modalExemplo")}
              >
                <ModalHeader toggle={this.toggle("modalExemplo")}>
                  Modal Exemplo
                </ModalHeader>
                <ModalBody className="modaladv">
                  <p>Tem certeza que deseja finalizar a consultoria?</p>
                </ModalBody>
                <ModalFooter>
                  <Button color="primary" onClick={this.toggle("modalExemplo")}>
                    Confirmar
                  </Button>
                </ModalFooter>
              </Modal>
            </Col>
            <Col md={9} style={{ height: "calc(100vh - 200px)" }}>
              <div className="ChatContent">{this.renderChat()}</div>
            </Col>
          </Row>
        </Container>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.setup.user.profile
});

export default connect(mapStateToProps, null)(Chat);
