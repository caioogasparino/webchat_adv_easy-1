import React, { Component } from "react";
import { connect } from "react-redux";
import {
  loginAdvogado,
  loginCliente,
  addProfile,
  loginFake,
} from "../redux/actions/login";
import { bindActionCreators } from "redux";

import {
  Container,
  Col,
  Row
} from "reactstrap";

import logo from "../assets/adveasy_fundoescuro.png";
import loadergif from "../assets/loader.gif";
import bgimg from "../assets/bglogin.jpg";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      senha: "",
      isLoad: false,
      type: "",
      message: "",
      error: false,
    };
  }

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };
  renderLoading = (type) => {
    if (this.state.isLoad && this.state.type === type) {
      return <img alt="loader" src={loadergif} />;
    }
  };
  renderErrors = () => {
    this.setState({ error: true });
    setTimeout(() => {
      this.setState({ error: false });
    }, 2000);
  };

  doLoginFake = () => {
    this.props.loginFake();
  };
  doLoginCliente = async () => {
    const { email, senha } = this.state;
    this.setState({ isLoad: true, type: "user", message: "" });
    const { success, message, data } = await this.props.loginCliente({
      email,
      senha,
    });
    if (success) {
      this.props.addProfile(data);
    } else {
      this.setState({ message: message });
    }
    this.setState({ isLoad: false });
  };
  doLoginAdvogado = async () => {
    const { email, senha } = this.state;
    if (email === "" || senha === "") {
      this.renderErrors();
    } else {
      this.setState({ isLoad: true, type: "adv", message: "" });
      const { success, message, data } = await this.props.loginAdvogado({
        email,
        senha,
      });

      if (success) {
        this.props.addProfile(data);
      } else {
        this.setState({ message: message });
      }
      this.setState({ isLoad: false });
    }
  };

  render() {
    const { email, senha, isLoad, error } = this.state;

    return (
      <div className="MainLoginContainer">
        <Container fluid className="vh-100">
          <Row className="h-100">
            <Col
              md={6}
              className="fill logincolbg"
              style={{ backgroundImage: `url(${bgimg})` }}
            >
              <div className="LoginTitle">
                <img className="LoginLogo" src={logo} alt="logo" />
              </div>
            </Col>
            <Col md={6} className="loginformcol">
              <div className="loginformcol__content">
                <h2 style={{ textAlign: "center" }}>Chat Web</h2>
                <div
                  className={
                    "form-group" + (error && !email ? " has-error" : "")
                  }
                >
                  <label htmlFor="email">E-mail</label>
                  {error && !email && (
                    <div className="help-block">
                      Campo e-mail não pode ficar vazio!
                    </div>
                  )}
                  <input
                    type="text"
                    className="form-control"
                    name="email"
                    value={email}
                    onChange={this.handleChange}
                  />
                </div>
                <div
                  className={
                    "form-group" + (error && !senha ? " has-error" : "")
                  }
                >
                  <label htmlFor="senha">Password</label>
                  {error && !senha && (
                    <div className="help-block">
                      Campo senha não pode ficar vazio!
                    </div>
                  )}
                  <input
                    type="password"
                    className="form-control"
                    name="senha"
                    value={senha}
                    onChange={this.handleChange}
                  />
                </div>
                {this.state.message && (
                  <div className="LoginError">{this.state.message}</div>
                )}
                <div className="form-btns">
                  <button
                    className="btn btn-primary"
                    onClick={this.doLoginFake}
                  >
                    {this.renderLoading("user")}Entrar como Cliente
                  </button>
                  <button
                    className="btn btn-secondary"
                    onClick={this.doLoginFake}
                  >
                    {this.renderLoading("adv")}Entrar como Advogado
                  </button>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      loginAdvogado,
      loginCliente,
      addProfile,
      loginFake,
    },
    dispatch
  );
export default connect(null, mapDispatchToProps)(Login);
