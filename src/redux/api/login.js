import { config } from "../../config";

export const doLoginAdvogados = async (params) => {
  console.log("params", params);
  try {
    const response = await fetch(
      `${config.BASE_URL_API}/advogados/validacao/login`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      }
    );
    if (response.status === 200) {
      return await response.json();
    }
    return { success: false, message: "Ocorreu um erro interno!" };
  } catch (error) {
    console.log("error", error);
    return { success: false, message: "Ocorreu um erro interno!" };
  }
};
export const doLoginClientes = async (params) => {
  try {
    const response = await fetch(
      `${config.BASE_URL_API}/clientes/validacao/login`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      }
    );
    if (response.status === 200) {
      return await response.json();
    }
    return { success: false, message: "Ocorreu um erro interno!" };
  } catch (error) {
    console.log("error", error);
    return { success: false, message: "Ocorreu um erro interno!" };
  }
};
