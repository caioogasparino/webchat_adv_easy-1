import axios from "axios";
import {config} from "../../config";

export const sendAvaliacao = async (params) => {
    try {
        const response = await fetch(`${config.BASE_URL_API}/consultorias/functions/enviarAvaliacao`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(params),
        });
        if (response.status === 200) {
            return await response.json();
        }
        return {success: false, message: 'Ocorreu um erro interno!'};
    } catch (error) {
        return {success: false, message: 'Ocorreu um erro interno!'};
    }
};