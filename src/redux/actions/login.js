import { doLoginAdvogados, doLoginClientes } from "../api/login";

export const loginFake = () => {
  let user = {
    profile: {
      id: 3,
      nome: "Carol",
      email: "c@comberweb.com.br",
      celular: "41988928484",
      cpf: "58657764016",
      data_nascimento: 723780000000,
      endereco_cliente_id: 4,
      senha:
        "eyJpdiI6IkliNGdoaE9hYkF5M0NxNVwvemlJQWVRPT0iLCJ2YWx1ZSI6IlRmaVNPTEpiZ2MxdlFMZUcrVlB6SXc9PSIsIm1hYyI6IjVhNDNjMjY0OWU0YjlkOWVjZTUwMWNmNWYyNDRhMDNmMjUyMGJhNTFhZDhlYTUwNTZhNzhkNDYyMjE3N2NjZDcifQ==",
      saldo: 0,
      uid: 321,
      cartao: [],
      endereco: {
        id: 3,
        endereco_completo: "Rua Teste",
        cidade: "Curitiba",
        estado: "Paraná",
        lat: "-25.4281647",
        long: "-49.2694684",
      },
    },
  };
  return {
    type: "ADD_USER",
    user,
  };
};
export const loginAdvogado = (params) => async (dispatch) => {
  console.log("action", params);
  try {
    const retorno = await doLoginAdvogados(params);
    console.log(retorno);
    if (retorno.success) {
      const { advogado } = retorno.data;
      await dispatch(addUser(advogado));
      return retorno;
    }
    return retorno;
  } catch (e) {
    console.log("e", e);
    return { success: false, message: "Ocorreu um erro interno!" };
  }
};
export const loginCliente = (params) => async (dispatch) => {
  try {
    const retorno = await doLoginClientes(params);

    if (retorno.success) {
      console.log(retorno.data);
      const { cliente } = retorno.data;
      await dispatch(addUser(cliente));
      return retorno;
    }
    return retorno;
  } catch (e) {
    console.log(e);
    return { success: false, message: "Ocorreu um erro interno!" };
  }
};

export const addUser = (user) => {
  return {
    type: "ADD_USER",
    user,
  };
};

export const deleteUser = () => {
  return {
    type: "DELETE_USER",
  };
};

export const addProfile = (profile) => {
  return {
    type: "ADD_PROFILE",
    profile: profile,
  };
};
