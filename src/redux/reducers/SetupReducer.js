const initialState = {
    userType: 0,
    user: '',
    adress: '',
    userLogged: false,
    profile:{}
};

export default function product(state = initialState, action) {
    switch (action.type) {
        case 'SELECT_USER':
            return Object.assign({}, state, {
                userType: action.typeId
            });
        case 'ADD_USER':
            return Object.assign({}, state, {
                user: action.user,
                userLogged: true,
            });
        case 'DELETE_USER':
            return Object.assign({}, state, {
                user: '',
                userLogged: false,
            });
        case 'ADD_PROFILE':
            return Object.assign({}, state, {
                profile: action.profile,
            });
        default:
            return state;
    }
}
